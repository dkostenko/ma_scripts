require 'rubyXL'
require 'json'
require 'active_support/all'
require 'HTTParty'
require 'open-uri'
require 'zip'
require 'httmultiparty'
require 'date'
## require 'FileUtils'


class DixyClient
  include HTTMultiParty
  base_uri 'https://dixy.forapp.mobi'
end

SESSION = "session_#{Time.now.to_i}_#{Random.rand(111)}"
TASKS_ID_COLUMN = 0
TASKS_REPORTS_COUNT_COLUMN = 1

FOLDER_SOURCES_OF_SOURCE_REPORTS = 'source_reports_sources'
FOLDER_TEMP = 'temp'
FOLDER_REPORT_TEMPLATE = 'report_template'
FOLDER_UPLOADED_ZIPS = 'uploaded_zips'


def run(options)
  start_time = Time.now
  puts "Сессия: #{SESSION}", "Начало выполнения: #{start_time}"
  
  clear_folder(FOLDER_UPLOADED_ZIPS)
  input_params = validate_input_options(options)
  current_user = authorize(input_params['phone'], input_params['password'])
  workbook = RubyXL::Parser.parse("create_reports.xlsx")
  data = workbook.worksheets[0].extract_data
  
  tasks_from_file = get_tasks_from_file(data)
  tasks = get_source_tasks(tasks_from_file, current_user)
  
  create_reports = create_reports_by_tasks(current_user, tasks)

  end_time = Time.now
  puts '', "Сессия: #{SESSION}", '=================', 'ОТЧЕТЫ СОЗДАЛИСЬ УСПЕШНО', "Начало выполнения: #{start_time}", "Конец выполнения: #{end_time}", "Общее время выполнения: #{end_time - start_time}сек."
  puts '','', "Всего создано отчетов: #{create_reports.values.flatten.size}", '', "Созданные отчеты: #{create_reports}"
end


def clear_folder(path)
  puts "CALL clear_folder(#{path})"
  FileUtils.rm_rf(path)
  FileUtils.mkdir(path)
  puts "RETURN clear_folder",''
end


def extract_mediafiles_from_source(local_temp_report_mediafiles_folder_path, photos_names)
  puts "CALL extract_mediafiles_from_source(#{local_temp_report_mediafiles_folder_path}, #{photos_names})"
  
  copied_photos_names = []
  
  # Копируем нужные фотографии.
  photos_names.each_with_index do |mediafile_name, index|
    copied_photo_name = "#{index}.jpg"
    path = "#{local_temp_report_mediafiles_folder_path}/#{mediafile_name}"

    if File.exist?(path)
      FileUtils.cp_r(path, "#{FOLDER_REPORT_TEMPLATE_MEDIAFILES}/#{copied_photo_name}")
      copied_photos_names.push(copied_photo_name)
    end
  end
  
  puts "RETURN extract_mediafiles_from_source #{copied_photos_names}",''
  return copied_photos_names
end


def unzip_source(local_file_path)
  puts "CALL unzip_source(#{local_file_path})"
  
  clear_folder(FOLDER_TEMP)
  clear_folder(FOLDER_REPORT_TEMPLATE)
  clear_folder(FOLDER_REPORT_TEMPLATE_MEDIAFILES)
  
  local_temp_zip_path = "#{FOLDER_TEMP}/tmp_source.zip"
  local_temp_report_folder_path = "#{FOLDER_TEMP}/report"
  local_temp_report_mediafiles_folder_path = "#{local_temp_report_folder_path}/media_files"
  json_file_path = "#{local_temp_report_folder_path}/report.json"
  
  # Копируем zip с исходником отчета во временную папку.
  FileUtils.cp_r(local_file_path, local_temp_zip_path)
  
  # Распаковываем zip с искходником отчета во временной папке.
  Zip::File.open(local_temp_zip_path) { |zip_file|
    zip_file.each { |f|
      f_path = File.join(local_temp_report_folder_path, f.name)
      FileUtils.mkdir_p(File.dirname(f_path))
      zip_file.extract(f, f_path) unless File.exist?(f_path)
    }
  }
  
  return local_temp_report_folder_path
  puts "RETURN unzip_source #{local_temp_report_folder_path}",''
end


def generate_report_json(task)
  puts "CALL generate_report_json(#{task})"
  
  ltd = task['ltd']
  lgt = task['lgt']
  
  report = {
    :report => {
      :steps => [{ 
        :position => 0, 
        :type => 'Scenario::Step::Question',
        :trouble => {},
        :elements => [
          {
            :key => 'photo_price_tov',
            :type => 'Element::MediaFile',
            :format => 'image',
            :human_name => 'Фото товара с ценником',
            :answer => {
              :value => [''],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'photo_price',
            :type => 'Element::MediaFile',
            :format => 'image',
            :human_name => 'Фото ценника',
            :answer => {
              :value => [''],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'price',
            :type => 'Element::Digits',
            :format => 'digits',
            :human_name => 'Цена регулярная',
            :answer => {
              :value => '0',
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'promo_price',
            :type => 'Element::Digits',
            :format => 'digits',
            :human_name => 'Цена акционная',
            :answer => {
              :value => '0',
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'photo_proizvod',
            :type => 'Element::MediaFile',
            :format => 'image',
            :human_name => 'Фото производителя',
            :answer => {
              :value => [''],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'photo_barcode',
            :type => 'Element::MediaFile',
            :format => 'image',
            :human_name => 'Фото штрих-кода',
            :answer => {
              :value => [''],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'barcode',
            :type => 'Element::Barcode',
            :format => 'barcode',
            :human_name => 'Штрих-код',
            :answer => {
              :value => [''],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'price_promo_name',
            :type => 'Element::Radio',
            :format => 'digits',
            :multiple_values => 'Нет;Лоток;3 х 2;Жабка;Спец. предложение;От производителя;Бонус;Купон;Подарок;Связка;Промоутер;Собирай баллы;Карта лояльности;Шелфтокер',
            :human_name => 'Примечание, название акции',
            :answer => {
              :value => [0],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'ctm',
            :type => 'Element::Radio',
            :format => 'radio',
            :multiple_values => 'Да;Нет',
            :human_name => 'СТМ',
            :answer => {
              :value => [1],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'yi2',
            :type => 'Element::String',
            :format => 'string',
            :human_name => 'Уровень 2',
            :answer => {
              :value => task['yi2'],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'yi3',
            :type => 'Element::String',
            :format => 'string',
            :human_name => 'Уровень 3',
            :answer => {
              :value => task['yi3'],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'yi4',
            :type => 'Element::String',
            :format => 'string',
            :human_name => 'Уровень 4',
            :answer => {
              :value => task['yi4'],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'yi5',
            :type => 'Element::String',
            :format => 'string',
            :human_name => 'Уровень 5',
            :answer => {
              :value => task['yi5'],
              :perform_time => Time.now.to_i,
              :lgt => lgt,
              :ltd => ltd,
            }
          }
        ]
      }] 
    }
  }

  puts "RETURN generate_report_json #{report}",''
  return report
end


def write_report_json(report)
  puts "CALL write_report_json(#{report})"
  
  path = "#{FOLDER_REPORT_TEMPLATE}/report.json"
  FileUtils.rm_rf(path)
  
  out_file = File.new(path, 'w')
  out_file.puts(report.to_json)
  out_file.close
  
  puts "RETURN write_report_json #{path}",''
  return path
end


def write_source_zip(report_json_path)
  puts "CALL write_source_zip(#{report_json_path})"
  
  path = "#{FOLDER_REPORT_TEMPLATE}/source.zip"
  FileUtils.rm_rf(path)
  
  Zip::File.open(path, Zip::File::CREATE) do |zipfile|
    zipfile.add('report.json', report_json_path)
  end
  
  puts "RETURN write_source_zip #{path}",''
  return path
end


def digest_report_creating_response(response)
  puts "CALL digest_report_creating_response(#{response})"
  report = {
    'id' => response['id'].to_s,
    'task_id' => response['task_id'].to_s,
  }
  
  throw 'Отсутвуeт ID отчета' if report['id'].blank?
  
  puts "RETURN digest_report_creating_response #{report}",''
  return report
end


def upload_report(current_user, zip_path, task_id)
  puts "CALL upload_report(#{current_user}, #{zip_path}, #{task_id})"
  
  query = {
    :report => {
      :moderator_comment => "automaticaly_created_report Сессия: #{SESSION}",
      :user_id => current_user['user_id'],
      :task_id => task_id,
      :source => File.open(zip_path)
    }
  }

  uri = "/api/hq/web/reports.json?response_format=full&authentication_token=#{current_user['authentication_token']}"
  puts "==> REQUEST POST to #{uri}"
    
  response = DixyClient.post(uri, :query => query)
  report = digest_report_creating_response(response)
    
  puts "RETURN upload_report #{report}",''
  return report
end

def create_reports_by_tasks(current_user, tasks)
  puts "CALL create_reports_by_tasks(current_user, tasks)"
  report_ids = {}
  reports = {}
  
  tasks.each do |task_id, task|
    report_ids[task_id] ||= []
    
    prepared_report_for_creating = generate_report_json(task)
    report_json_for_creating_path = write_report_json(prepared_report_for_creating)
    zip_for_creating_path = write_source_zip(report_json_for_creating_path)
    
    (1..task['reports_count']).each do |count|
      created_report = upload_report(current_user, zip_for_creating_path, task_id)
      
      report_ids[task_id].push(created_report['id'])
    end
  end
  
  puts "RETURN create_reports_by_column [report_ids, reports]",''
  return report_ids
end

def get_tasks_from_file(data)
  puts "CALL get_tasks_from_file(data)"
  tasks = {}
  
  data.each do |row|
    next if row.blank? || row[TASKS_ID_COLUMN].blank?
    
    tasks[row[TASKS_ID_COLUMN]] ||= {}
    tasks[row[TASKS_ID_COLUMN]].merge!({
      'reports_count' => row[TASKS_REPORTS_COUNT_COLUMN]
    })
  end
  
  puts "RETURN get_tasks_from_file",''
  return tasks
end


def digest_task_response(response)
  puts "CALL digest_task_response(#{response})"
  variables = response['variables'] || {}
  
  task = {
    'id' => response['id'].to_s,
    'ltd' => response['ltd'],
    'lgt' => response['lgt'],
    'yi2' => variables['yi2'],
    'yi3' => variables['yi3'],
    'yi4' => variables['yi4'],
    'yi5' => variables['yi5']
  }
  
  throw 'Отсутвуeт ID задания' if task['id'].blank?
  
  puts "RETURN digest_task_response #{task}",''
  return task
end


def get_source_tasks(tasks_from_file, current_user)
  puts "CALL get_source_tasks(#{tasks_from_file})"
  tasks = {}
  
  tasks_from_file.each do |id, reports_count|
    next if tasks[id].present?
    
    uri = "/api/hq/web/tasks/#{id}.json?authentication_token=#{current_user['authentication_token']}"
    puts "==> REQUEST GET to #{uri}"
    response = DixyClient.get(uri)
     
    task = tasks_from_file[id].merge(digest_task_response(response))
    tasks[task['id']] = task
  end
  
  puts "RETURN get_source_tasks #{tasks}",''
  return tasks
end

def authorize(phone, password)
  puts "CALL authorize(#{phone}, #{password})"

  query = { 
    :password => password, 
    :phone => phone, 
    :platform => 'external_system', 
    :uuid => "ygfbiu24bf83bibG#78v&D3bu2ybd3budi23ugd88y9-#{Time.now.to_i}-#{Random.rand(111)}"
  }
  
  uri = '/api/hq/web/sessions.json'
  puts "==> REQUEST POST to #{uri}"
  session_create_response = DixyClient.post(uri, :query => query)
  
  user = {
    'authentication_token' => session_create_response['device']['authentication_token'],
    'user_id' => session_create_response['user']['id']
  }
  
  puts "RETURN authorize #{user}",''
  
  return user
end


def validate_input_options(args)
  puts "CALL validate_input_options(#{args})"

  options = {
    'phone' => args[0],
    'password' => args[1],
  }

  if options['phone'].blank? || options['password'].blank?
    throw 'Отсутсвуют необходимые параметры. phone password'
  end

  puts "RETURN validate_input_options #{options}",''

  return options
end


run(ARGV)
