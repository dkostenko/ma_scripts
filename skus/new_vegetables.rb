require 'rubyXL'
require 'json'
require 'active_support/all'
require 'HTTParty'
require 'open-uri'
require 'zip'
require 'httmultiparty'
require 'date'
## require 'FileUtils'


class DixyClient
  include HTTMultiParty
  base_uri 'https://dixy.forapp.mobi'
end


SESSION = "session_#{Time.now.to_i}_#{Random.rand(111)}"
REPORTS_CELL_STARTING_COLUMN = 11
REPORT_IDS_ROW = 0
SKUS_STARTING_ROW = 2
SKU_YI2_COLUMN = 0
SKU_YI3_COLUMN = 1
SKU_YI4_COLUMN = 2
SKU_YI5_COLUMN = 3
SKU_DIXY_CODE = 5
SKU_BARCODE = 4
SKU_TITLE_COLUMN = 6
SKU_MANUFACTURER_COLUMN = 7
SKU_WEIGHT_COLUMN = 8
SKU_SALES_METHOD_COLUMN = 9
SKU_CALIBER_COLUMN = 10
FOLDER_SOURCES_OF_SOURCE_REPORTS = 'source_reports_sources'
FOLDER_TEMP = 'temp'
FOLDER_REPORT_TEMPLATE = 'report_template'
FOLDER_REPORT_TEMPLATE_MEDIAFILES = "#{FOLDER_REPORT_TEMPLATE}/media_files"
FOLDER_UPLOADED_ZIPS = 'uploaded_zips'


def run(options)
  start_time = Time.now
  puts "Сессия: #{SESSION}", "Начало выполнения: #{start_time}"
  
  clear_folder(FOLDER_UPLOADED_ZIPS)
  @input_params = validate_input_options(options)
  current_user = authorize(@input_params['phone'], @input_params['password'])
  workbook = RubyXL::Parser.parse("asd.xlsx")
  data = workbook.worksheets[0].extract_data
  
  skus = get_skus(data)
  source_report_ids = get_source_report_ids(current_user, data)
  source_reports = get_source_reports(source_report_ids, current_user)
  sourse_tasks = get_source_tasks(source_reports, current_user)
  
  target_tasks_ids, target_tasks, source_report_ids_to_created_tasks_ids = search_or_create_tasks(current_user, sourse_tasks, source_reports, source_report_ids)
  
  created_reports_ids, created_reports = create_reports_by_column(current_user, data, skus, source_report_ids, source_report_ids_to_created_tasks_ids, source_reports)

  end_time = Time.now
  puts '', "Сессия: #{SESSION}", '=================', 'ОТЧЕТЫ СОЗДАЛИСЬ УСПЕШНО', "Начало выполнения: #{start_time}", "Конец выполнения: #{end_time}", "Общее время выполнения: #{end_time - start_time}сек."
  puts '', '', "Всего целевых заданий: #{target_tasks_ids.size}", '', "Целевые задания: #{target_tasks_ids}", '', '', "Всего создано отчетов: #{created_reports_ids.size}", '', "Созданные отчеты: #{created_reports_ids}"
end


def download_source_of_source_report(current_user, created_task_id, sourse_report)
  puts "CALL download_source_of_source_report(current_user, created_task_id, sourse_report)"
  
  local_file_name = "#{FOLDER_SOURCES_OF_SOURCE_REPORTS}/#{sourse_report['id']}.zip"
  
  # Если сорс уже скачан, то возращаем относительный путь до него.
  if File.exist?(local_file_name)
    puts "==> FILE ALREADY EXIST #{local_file_name}"
    puts "RETURN download_source_of_source_report #{local_file_name}",''
    return local_file_name
  end
  
  # Если нет папки для сорсов, то создаем папку.
  FileUtils.mkdir(FOLDER_SOURCES_OF_SOURCE_REPORTS) unless File.exist?(FOLDER_SOURCES_OF_SOURCE_REPORTS)
  
  # Качаем сорс отчета.
  puts "==> REQUEST GET to #{sourse_report['source_url']}"
  open(local_file_name, 'wb') do |fo|
    fo.print open(sourse_report['source_url']).read
  end
  
  puts "RETURN download_source_of_source_report #{local_file_name}",''
  return local_file_name
end


def clear_folder(path)
  puts "CALL clear_folder(#{path})"
  FileUtils.rm_rf(path)
  FileUtils.mkdir(path)
  puts "RETURN clear_folder",''
end


def extract_mediafiles_from_source(local_temp_report_mediafiles_folder_path, photos_names = [])
  puts "CALL extract_mediafiles_from_source(#{local_temp_report_mediafiles_folder_path}, #{photos_names})"
  
  copied_photos_names = []
  
  # Копируем нужные фотографии.
  photos_names.each_with_index do |mediafile_name, index|
    copied_photo_name = "#{index}.jpg"
    path = "#{local_temp_report_mediafiles_folder_path}/#{mediafile_name}"

    if File.exist?(path)
      FileUtils.cp_r(path, "#{FOLDER_REPORT_TEMPLATE_MEDIAFILES}/#{copied_photo_name}")
      copied_photos_names.push(copied_photo_name)
    end
  end
  
  puts "RETURN extract_mediafiles_from_source #{copied_photos_names}",''
  return copied_photos_names
end


def unzip_source(local_file_path)
  puts "CALL unzip_source(#{local_file_path})"
  
  clear_folder(FOLDER_TEMP)
  clear_folder(FOLDER_REPORT_TEMPLATE)
  clear_folder(FOLDER_REPORT_TEMPLATE_MEDIAFILES)
  
  local_temp_zip_path = "#{FOLDER_TEMP}/tmp_source.zip"
  local_temp_report_folder_path = "#{FOLDER_TEMP}/report"
  local_temp_report_mediafiles_folder_path = "#{local_temp_report_folder_path}/media_files"
  json_file_path = "#{local_temp_report_folder_path}/report.json"
  
  # Копируем zip с исходником отчета во временную папку.
  FileUtils.cp_r(local_file_path, local_temp_zip_path)
  
  # Распаковываем zip с искходником отчета во временной папке.
  Zip::File.open(local_temp_zip_path) { |zip_file|
    zip_file.each { |f|
      if %w(.jpg .png .bmp .gif).include?(File.extname(f.name))
        f_path = File.join(local_temp_report_folder_path, 'media_files', Pathname.new(f.name).basename)
      else
        f_path = File.join(local_temp_report_folder_path, f.name)
      end

      FileUtils.mkdir_p(File.dirname(f_path))
      zip_file.extract(f, f_path) unless File.exist?(f_path)
    }
  }
  
  return local_temp_report_folder_path
  puts "RETURN unzip_source #{local_temp_report_folder_path}",''
end


def generate_report_json(price, promo_price, answer, sku, photos_names)
  puts "CALL generate_report_json(#{price}, #{answer}, #{sku}, #{photos_names})"
  
  lgt = answer['lgt']
  ltd = answer['ltd']
  perform_time = answer['perform_time']
  
  report = {
    :report => {
      :steps => [{ 
        :position => 0, 
        :type => 'Scenario::Step::Question',
        :trouble => {},
        :elements => [
          {
            :key => 'photo_price',
            :type => 'Element::MediaFile',
            :format => 'image',
            :human_name => 'Фото ценника',
            :answer => {
              :value => photos_names,
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'photo_price_tov',
            :type => 'Element::MediaFile',
            :format => 'image',
            :human_name => 'Фото товара',
            :answer => {
              :value => photos_names,
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'price',
            :type => 'Element::Digits',
            :format => 'digits',
            :human_name => 'Цена регулярная',
            :answer => {
              :value => price || '0',
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'promo_price',
            :type => 'Element::Digits',
            :format => 'digits',
            :human_name => 'Цена акционная',
            :answer => {
              :value => promo_price || '0',
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'barcode',
            :type => 'Element::Barcode',
            :format => 'barcode',
            :human_name => 'Штрихкод',
            :answer => {
              :value => ["#{sku['barcode']}"],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'price_promo_name',
            :type => 'Element::Radio',
            :format => 'digits',
            :multiple_values => 'Нет;Лоток;3 х 2;Жабка;Спец. предложение;От производителя;Бонус;Купон;Подарок;Связка;Промоутер;Собирай баллы;Карта лояльности;Шелфтокер',
            :human_name => 'Примечание, название акции',
            :answer => {
              :value => [0],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'ctm',
            :type => 'Element::Radio',
            :format => 'radio',
            :multiple_values => 'Да;Нет',
            :human_name => 'СТМ',
            :answer => {
              :value => [1],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'yi2',
            :type => 'Element::String',
            :format => 'string',
            :human_name => 'Уровень 2',
            :answer => {
              :value => sku['yi2'],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'yi3',
            :type => 'Element::String',
            :format => 'string',
            :human_name => 'Уровень 3',
            :answer => {
              :value => sku['yi3'],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'yi4',
            :type => 'Element::String',
            :format => 'string',
            :human_name => 'Уровень 4',
            :answer => {
              :value => sku['yi4'],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'yi5',
            :type => 'Element::String',
            :format => 'string',
            :human_name => 'Уровень 5',
            :answer => {
              :value => sku['yi5'],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'dixy_title',
            :type => 'Element::String',
            :format => 'string',
            :human_name => '',
            :answer => {
              :value => sku['dixy_title'],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'dixy_manufacturer',
            :type => 'Element::String',
            :format => 'string',
            :human_name => '',
            :answer => {
              :value => sku['dixy_manufacturer'],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          },
          {
            :key => 'dixy_code',
            :type => 'Element::String',
            :format => 'string',
            :human_name => '',
            :answer => {
              :value => sku['dixy_code'],
              :perform_time => perform_time,
              :lgt => lgt,
              :ltd => ltd,
            }
          }
        ]
      }] 
    }
  }

  # Выбираю ответ, которы внесен в .xlsx
  weight_values = ['УП', 'Введите вес товара в ГРАММАХ']
  weight_selected_value = Hash[weight_values.map.with_index.to_a]["#{sku['weight']}"]
  if weight_selected_value.blank?
    weight_selected_value = 0
    weight_values = ["#{sku['weight']}", 'УП']
  else
    weight_selected_value = weight_selected_value
  end
  report[:report][:steps][0][:elements].push({
    :key => 'weight',
    :type => 'Element::Radio',
    :format => 'radio',
    :multiple_values => weight_values.join(";"),
    :human_name => 'Вес',
    :answer => {
      :value => [weight_selected_value],
      :perform_time => perform_time,
      :lgt => lgt,
      :ltd => ltd,
    }
  })

  # Выбираю ответ, которы внесен в .xlsx
  sales_method_values = ['ШТ', 'ВЕС']
  sales_method_selected_value = Hash[sales_method_values.map.with_index.to_a][answer["#{sku['sales_method']}"]]
  if sales_method_selected_value.blank?
    sales_method_selected_value = 0
  else
    sales_method_selected_value = sales_method_selected_value
  end
  report[:report][:steps][0][:elements].push({
    :key => 'sales_method',
    :type => 'Element::Radio',
    :format => 'radio',
    :multiple_values => sales_method_values.join(";"),
    :human_name => 'Способ продажи',
    :answer => {
      :value => [sales_method_selected_value],
      :perform_time => perform_time,
      :lgt => lgt,
      :ltd => ltd,
    }
  })

  # Выбираю ответ, которы внесен в .xlsx
  caliber_values = ['М', 'С', 'К']
  caliber_selected_value = Hash[caliber_values.map.with_index.to_a]["#{sku['caliber']}"]
  if caliber_selected_value.blank?
    caliber_selected_value = 0
  else
    caliber_selected_value = caliber_selected_value
  end
  report[:report][:steps][0][:elements].push({
    :key => 'caliber',
    :type => 'Element::Radio',
    :format => 'radio',
    :multiple_values => caliber_values.join(";"),
    :human_name => 'Калибр',
    :answer => {
      :value => [caliber_selected_value],
      :perform_time => perform_time,
      :lgt => lgt,
      :ltd => ltd,
    }
  })

  puts "RETURN generate_report_json #{report}",''
  return report
end


def write_report_json(report)
  puts "CALL write_report_json(#{report})"
  
  path = "#{FOLDER_REPORT_TEMPLATE}/report.json"
  FileUtils.rm_rf(path)
  
  out_file = File.new(path, 'w')
  out_file.puts(report.to_json)
  out_file.close
  
  puts "RETURN write_report_json #{path}",''
  return path
end


def write_source_zip(report_json_path, photos_names)
  puts "CALL write_source_zip(#{report_json_path}, #{photos_names})"
  
  path = "#{FOLDER_REPORT_TEMPLATE}/source.zip"
  FileUtils.rm_rf(path)
  
  Zip::File.open(path, Zip::File::CREATE) do |zipfile|
    zipfile.add('report.json', report_json_path)

    photos_names.each_with_index do |file, image_index|
      zipfile.add("media_files/#{file}", "#{FOLDER_REPORT_TEMPLATE_MEDIAFILES}/#{file}")
    end
  end
  
  puts "RETURN write_source_zip #{path}",''
  return path
end


def digest_report_creating_response(response)
  puts "CALL digest_report_creating_response(#{response})"
  report = {
    'id' => response['id'].to_s,
    'task_id' => response['task_id'].to_s,
  }
  
  throw 'Отсутвуeт ID отчета' if report['id'].blank?
  
  puts "RETURN digest_report_creating_response #{report}",''
  return report
end


def upload_report(current_user, zip_path, excel_row_index, excel_col_index, source_report_id, target_task_id)
  puts "CALL upload_report(#{current_user}, #{zip_path}, #{excel_row_index}, #{excel_col_index}, #{source_report_id}, #{target_task_id})"
  
  query = {
    :report => {
      :moderator_comment => "automaticaly_created_report Сессия: #{SESSION} Координаты ячейки в файле(счет начинается с 0): [#{excel_row_index}; #{excel_col_index}]; созадно по отчету ##{source_report_id}",
      :user_id => current_user['user_id'],
      :task_id => target_task_id,
      :source => File.open(zip_path)
    }
  }

  uri = "/api/hq/web/reports.json?response_format=full&authentication_token=#{current_user['authentication_token']}"
  puts "==> REQUEST POST to #{uri}"
  response = DixyClient.post(uri, :query => query)
  report = digest_report_creating_response(response)

  FileUtils.cp_r(zip_path, "#{FOLDER_UPLOADED_ZIPS}/r#{excel_row_index}_c#{excel_col_index}.zip")
  
  puts "RETURN upload_report #{report}",''
  return report
end


def extract_photo_category_afar_answer_from_source(report_json_path)
  puts "CALL extract_photo_category_afar_answer_from_source(#{report_json_path})"
  
  photo_category_afar_answer = nil
  
  # Считываем report.json.
  if File.exist?(report_json_path)
    report_json_file = File.read(report_json_path)
    json = JSON.parse(report_json_file)
    
    # Вытаскиваем массив с нужными фотографиями.
    elements = json['report']['steps'][0]['elements']
    photo_category_afar_element = elements.find{|a| a['key'] == 'photo_category_afar'}
    photo_category_afar_answer = photo_category_afar_element['answer'] if photo_category_afar_element.present?
  end
  
  puts "RETURN extract_photo_category_afar_answer_from_source #{photo_category_afar_answer}",''
  return photo_category_afar_answer
end


def extract_photo_category_afar_answer_from_source_report(report)
  puts "CALL extract_photo_category_afar_answer_from_source_report(#{report})"
  
  answer = {
    'value' => [],
    'lgt' => report['lgt'],
    'ltd' => report['ltd'],
    'perform_time' => report['perform_time']
  }
  
  report['images'].each_with_index do |image, index|
    filepath = "#{FOLDER_TEMP}/report/media_files"
    filename = "#{index}.jpg"
    
    # Качаем сорс отчета.
    puts "==> REQUEST GET to #{image['url']}"
    open("#{filepath}/#{filename}", 'wb') do |fo|
      fo.print open(image['url']).read
    end
    
    answer['value'].push(filename)
  end
  
  puts "RETURN extract_photo_category_afar_answer_from_source_report #{answer}",''
  return answer
end


def extract_photo_category_near_answer_from_source(report_json_path)
  puts "CALL extract_photo_category_near_answer_from_source(#{report_json_path})"
  
  photo_category_near_answer = nil
  
  # Считываем report.json.
  if File.exist?(report_json_path)
    report_json_file = File.read(report_json_path)
    json = JSON.parse(report_json_file)
    
    # Вытаскиваем массив с нужными фотографиями.
    elements = json['report']['steps'][0]['elements']
    photo_category_near_element = elements.find{|a| a['key'] == 'photo_category_near'}
    photo_category_near_answer = photo_category_near_element['answer'] if photo_category_near_element.present?
  end
  
  puts "RETURN extract_photo_category_near_answer_from_source #{photo_category_near_answer}",''
  return photo_category_near_answer
end


def extract_photo_category_near_answer_from_source_report(report)
  puts "CALL extract_photo_category_near_answer_from_source_report(#{report})"
  
  answer = {
    'value' => [],
    'lgt' => report['lgt'],
    'ltd' => report['ltd'],
    'perform_time' => report['perform_time']
  }
  
  report['images'].each_with_index do |image, index|
    filepath = "#{FOLDER_TEMP}/report/media_files"
    filename = "#{index}.jpg"
    
    # Качаем сорс отчета.
    puts "==> REQUEST GET to #{image['url']}"
    open("#{filepath}/#{filename}", 'wb') do |fo|
      fo.print open(image['url']).read
    end
    
    answer['value'].push(filename)
  end
  
  puts "RETURN extract_photo_category_near_answer_from_source_report #{answer}",''
  return answer
end


def create_reports_by_column(current_user, data, skus, source_report_ids, source_report_ids_to_created_tasks_ids, source_reports)
  puts "CALL create_reports_by_column(current_user, data, skus, source_report_ids, source_report_ids_to_created_tasks_ids, source_reports)"
  report_ids = []
  reports = {}
  
  source_report_ids.each_with_index do |source_report_id, col_index|
    excel_col_index = (col_index * 2) + REPORTS_CELL_STARTING_COLUMN
    
    created_task_id = source_report_ids_to_created_tasks_ids[source_report_id]
    sourse_report = source_reports[source_report_id]
    
    local_file_path = download_source_of_source_report(current_user, created_task_id, sourse_report)
    
    unzip_report_path = unzip_source(local_file_path)
    photo_category_afar_answer = extract_photo_category_afar_answer_from_source("#{unzip_report_path}/report.json")
    photo_category_afar_answer = extract_photo_category_afar_answer_from_source_report(sourse_report) if photo_category_afar_answer.blank?

    photo_category_near_answer = extract_photo_category_near_answer_from_source("#{unzip_report_path}/report.json")
    photo_category_near_answer = extract_photo_category_near_answer_from_source_report(sourse_report) if photo_category_near_answer.blank?
    
    answer = photo_category_afar_answer.present? ? photo_category_afar_answer : photo_category_near_answer
      
    # local_temp_report_mediafiles_folder_path, local_file_path, photos_names
    photos_names = extract_mediafiles_from_source("#{unzip_report_path}/media_files", photo_category_afar_answer['value'] | photo_category_near_answer['value'])
    
    skus.each_with_index do |sku, row_index|
      excel_row_index = row_index + SKUS_STARTING_ROW
      
      price = data[excel_row_index][excel_col_index]
      promo_price = data[excel_row_index][excel_col_index + 1]
      next if price.blank? && promo_price.blank?
      
      prepared_report_for_creating = generate_report_json(price, promo_price, answer, skus[row_index], photos_names)
      report_json_for_creating_path = write_report_json(prepared_report_for_creating)
      zip_for_creating_path = write_source_zip(report_json_for_creating_path, photos_names)
      
      created_report = upload_report(current_user, zip_for_creating_path, excel_row_index, excel_col_index, source_report_id, created_task_id)
      reports[created_report['id']] = created_report
      report_ids.push(created_report['id'])
    end
  end
  
  puts "RETURN create_reports_by_column [report_ids, reports]",''
  return [report_ids, reports]
end


def digest_task_creating_response(response)
  puts "CALL digest_task_creating_response(#{response})"
  task = {
    'id' => response['id'].to_s
  }
  
  throw 'Отсутвуeт ID задания' if task['id'].blank?
  
  puts "RETURN digest_task_creating_response #{task}",''
  return task
end


def create_task(current_user, source_task)
  puts "CALL create_task(current_user, sourse_tasks)"
  
  query = { 
    'task' => { 
      'title' => source_task['title'], 
      'description' => "Это задание создано автоматически, как клон задания #{source_task['id']} #{SESSION}", 
      'geo_object_id' => source_task['geo_object']['id'], 
      'project_id' => @input_params['target_project_id'],
      'wave' => source_task['wave']
    }
  }
  
  uri = "/api/hq/web/tasks.json?response_format=full&authentication_token=#{current_user['authentication_token']}"
  puts "==> REQUEST POST to #{uri}"
  response = DixyClient.post(uri, :query => query)
  task = digest_task_creating_response(response)
  
  puts "RETURN create_task(current_user, source_task)"
  return task
end


def get_skus(data)
  puts "CALL get_skus(data)"
  skus = []
  
  (SKUS_STARTING_ROW..data.size).each do |row_index|
    row = data[row_index]
    
    next if row.blank? || row[SKU_TITLE_COLUMN].blank?
    
    skus.push({
      'yi2' => row[SKU_YI2_COLUMN],
      'yi3' => row[SKU_YI3_COLUMN],
      'yi4' => row[SKU_YI4_COLUMN],
      'yi5' => row[SKU_YI5_COLUMN],
      'barcode' => row[SKU_BARCODE],
      'dixy_code' => row[SKU_DIXY_CODE],
      'dixy_title' => row[SKU_TITLE_COLUMN],
      'dixy_manufacturer' => row[SKU_MANUFACTURER_COLUMN],
      'caliber' => row[SKU_CALIBER_COLUMN],
      'weight' => row[SKU_WEIGHT_COLUMN],
      'sales_method' => row[SKU_SALES_METHOD_COLUMN]
    })


    # SKU_MANUFACTURER_COLUMN
    # SKU_WEIGHT_COLUMN
    # SKU_SALES_METHOD_COLUMN
    # SKU_CALIBER_COLUMN

  end
  
  puts "RETURN get_skus #{skus}",''
  return skus
end


def digest_task_response(response)
  puts "CALL digest_task_response(#{response})"
  task = {
    'id' => response['id'].to_s,
    'title' => response['title'],
    'geo_object' => {
      'id' => response['geo_object']['id'].to_s
    },
    'wave' => response['wave'].to_s
  }
  
  throw 'Отсутвуeт ID задания' if task['id'].blank?
  
  puts "RETURN digest_task_response #{task}",''
  return task
end


def get_source_tasks(reports, current_user)
  puts "CALL get_source_tasks(#{reports}, #{current_user})"
  tasks = {}
  
  reports.each do |id, report|
    next if tasks[report['task_id']].present?
    
    uri = "/api/hq/web/tasks/#{report['task_id']}.json?authentication_token=#{current_user['authentication_token']}"
    puts "==> REQUEST GET to #{uri}"
    response = DixyClient.get(uri)

    task = digest_task_response(response)
    
    tasks[task['id']] = task
  end
  
  puts "RETURN get_source_tasks #{tasks}",''
  return tasks
end


def search_task_response(response)
  puts "CALL search_task_response(#{response})"
  
  task = {}
  
  if @input_params['clone_validation']
    if response['tasks'].size == 1
      task = {
        'id' => response['tasks'][0]['id'].to_s,
        'title' => response['tasks'][0]['title'],
        'geo_object' => {
          'id' => response['tasks'][0]['geo_object']['id'].to_s
        },
        'wave' => response['tasks'][0]['wave'].to_s
      }
      
      throw 'Отсутвуeт ID задания' if task['id'].blank?
    elsif response['tasks'].size > 0
      throw 'Найдено больше одного задания'
    end
  else
    if response['tasks'].present?
      task = {
        'id' => response['tasks'][0]['id'].to_s,
        'title' => response['tasks'][0]['title'],
        'geo_object' => {
          'id' => response['tasks'][0]['geo_object']['id'].to_s
        },
        'wave' => response['tasks'][0]['wave'].to_s
      }
      
      throw 'Отсутвуeт ID задания' if task['id'].blank?
    end
  end
  
  puts "RETURN search_task_response #{task}",''
  return task
end



def search_or_create_tasks(current_user, sourse_tasks, source_reports, source_report_ids)
  puts "CALL search_or_create_tasks(#{current_user}, #{sourse_tasks}, #{source_reports}, #{source_report_ids})"
  tasks_ids = []
  tasks = {}
  source_report_ids_to_created_tasks_ids = {}
  
  source_report_ids.each do |report_id|
    source_report = source_reports[report_id]
    source_task = sourse_tasks[source_report['task_id']]

    query = {
      'search' => {
        'title_cont' => source_task['title'],
        'wave_cont' => source_task['wave'],
        'geo_object_id_eq' => source_task['geo_object']['id'],
        'project_id_eq' => @input_params['target_project_id']
      }
    }
    
    uri = "/api/hq/web/tasks/search.json?authentication_token=#{current_user['authentication_token']}"
    puts "==> REQUEST POST to #{uri}"
    response = DixyClient.post(uri, :query => query)

    task = if search_task_response(response).blank? then create_task(current_user, source_task) else search_task_response(response) end;

    next if task.blank?
    
    tasks_ids.push(task['id'])
    tasks[task['id']] = task
    source_report_ids_to_created_tasks_ids[report_id] = task['id']
  end
  
  puts "RETURN search_or_create_tasks [#{tasks_ids}, #{tasks}, #{source_report_ids_to_created_tasks_ids}]",''
  return [tasks_ids, tasks, source_report_ids_to_created_tasks_ids]
end


def digest_report_response(response)
  puts "CALL digest_report_response(#{response})"
  report = {
    'id' => response['id'].to_s,
    'source_url' => response['source']['url'],
    'task_id' => response['task_id'].to_s,
    'images' => [],
    'perform_time' => nil
  }
  
  throw 'Отсутвуют ID отчета' if report['id'].blank?
  throw 'Отсутвуют ID задания' if report['task_id'].blank?

  answer = response['answers'].find{|a| a['key'] == 'photo_category_afar' || a['key'] == 'photo_category_near' }
  throw 'Отсутвует ответ с фотографиями' if answer.blank?

  perform_time = DateTime.parse(answer['performed_at'])
  throw 'Отсутвует дата создания ответа' if perform_time.blank?
  report['perform_time'] = Time.new(perform_time.year, perform_time.month, perform_time.day, perform_time.hour, perform_time.minute, perform_time.second).to_i
  
  throw 'Отсутвуют фотографии в отчете' if answer['images'].size == 0
  report['images'] = answer['images']
  
  report['lgt'] = answer['lgt']
  report['ltd'] = answer['ltd']
  
  puts "RETURN digest_report_response #{report}",''
  return report
end


def get_source_reports(report_ids, current_user)
  puts "CALL get_source_reports(#{report_ids}, #{current_user})"
  reports = {}
  
  report_ids.each do |report_id|
    next if reports[report_id].present?
    
    uri = "/api/hq/web/reports/#{report_id}.json?authentication_token=#{current_user['authentication_token']}"
    puts "==> REQUEST GET to #{uri}"
    response = DixyClient.get(uri)
    report = digest_report_response(response)
    
    reports[report['id']] = report
  end
  
  puts "RETURN get_source_reports #{reports}",''
  return reports
end


def get_source_report_ids(current_user, data)
  puts "CALL get_source_report_ids(#{current_user}, data)"
  report_ids = []
  cols = data[REPORT_IDS_ROW]
  
  (REPORTS_CELL_STARTING_COLUMN..cols.size).each do |index|
    report_id = cols[index].to_s
  
    next if report_id.blank?
  
    if report_ids.include?(report_id)
      puts "Дубль: #{report_id}"
      throw 'Присутствуют дублирующиеся отчеты в заголовках'
    end
  
    report_ids.push(report_id)
  end
  
  puts "RETURN get_source_report_ids #{report_ids}",''
  return report_ids
end


def authorize(phone, password)
  puts "CALL authorize(#{phone}, #{password})"

  query = { 
    :password => password, 
    :phone => phone, 
    :platform => 'external_system', 
    :uuid => "ygfbiu24bf83bibG#78v&D3bu2ybd3budi23ugd88y9-#{Time.now.to_i}-#{Random.rand(111)}"
  }
  
  uri = '/api/hq/web/sessions.json'
  puts "==> REQUEST POST to #{uri}"
  session_create_response = DixyClient.post(uri, :query => query)
  
  user = {
    'authentication_token' => session_create_response['device']['authentication_token'],
    'user_id' =>session_create_response['user']['id']
  }
  
  puts "RETURN authorize #{user}",''
  
  return user
end


def validate_input_options(args)
  puts "CALL validate_input_options(#{args})"

  options = {
    'phone' => args[0],
    'password' => args[1],
    'target_project_id' => args[2],
    'clone_validation' => args[3]
  }

  if options['phone'].blank? || options['password'].blank? || options['target_project_id'].blank?
    throw 'Отсутсвуют необходимые параметры. phone password target_project_id'
  end

  puts "RETURN validate_input_options #{options}",''

  return options
end


run(ARGV)
